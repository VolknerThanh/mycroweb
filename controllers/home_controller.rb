require 'application_controller'
require 'trainee'

class HomeController < ApplicationController

  def index
    render :index, { trainees: Trainee.all }
  end

  def about
    render :about
  end

  def page_not_found
    render :page_not_found
  end
end
